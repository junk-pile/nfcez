from nfc import ContactlessFrontend
from nfc.clf import RemoteTarget
from nfc.tag import activate
from .logging import logger


class TagConnection(object):
    def __init__(self, bitrate_formats=["106A"]):
        self.clf = ContactlessFrontend("usb")
        self.bitrate_formats = bitrate_formats

    def __enter__(self):
        logger.info("Listening for connection...")
        while True:
            target = self.clf.sense(
                *[RemoteTarget(bf) for bf in self.bitrate_formats]
            )
            if target is not None:
                logger.info("Connection made!")
                return activate(self.clf, target)

    def __exit__(self, type, value, traceback):
        if any([type, value, traceback]):
            logger.info("Exception occurred:", type, value, traceback)
        self.clf.close()
