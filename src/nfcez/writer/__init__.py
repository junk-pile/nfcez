from nfcez import TagConnection, logger


def format():
    with TagConnection() as tag:
        logger.info("Formatting...")
        tag.format()
        logger.info("Done.")


def set_records(records):
    with TagConnection() as tag:
        logger.info("Overwriting records...")
        tag.ndef.records = records
        logger.info("Done.")


def add_records(records):
    with TagConnection() as tag:
        logger.info("Appending record...")
        tag.ndef.records += records
        logger.info("Done.")
