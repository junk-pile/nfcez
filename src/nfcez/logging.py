from .settings import (
    NFCEZ_LOG_LEVEL,
    NFCEZ_LOG_FORMAT,
    NFCEZ_LOG_FORMAT_STYLE,
    NFCEZ_DISABLE_LOGGING,
)
from sys import stdout

import logging

logging.basicConfig(level=getattr(logging, NFCEZ_LOG_LEVEL.upper()))

logger = logging.getLogger("nfcez")
logger.propagate = False

if not NFCEZ_DISABLE_LOGGING:
    handler = logging.StreamHandler(stdout)
    handler.setLevel(getattr(logging, NFCEZ_LOG_LEVEL.upper()))

    formatter = logging.Formatter(
        NFCEZ_LOG_FORMAT, style=NFCEZ_LOG_FORMAT_STYLE
    )
    handler.setFormatter(formatter)

    logger.addHandler(handler)
