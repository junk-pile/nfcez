from nfcez import TagConnection, logger


def get_records():
    with TagConnection() as tag:
        logger.info("Fetching records...")
        return tag.ndef.records
        logger.info("Done.")


def get_record_names():
    return [r.name for r in get_records()]


def get_record(name):
    records = {r.name: r for r in get_records()}
    try:
        return records[name]
    except KeyError:
        return None
