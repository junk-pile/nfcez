import ndef


class BytesRecord(ndef.record.GlobalRecord):
    _type = "urn:nfc:ext:nfcez:x-binary"
    _decode_min_payload_length = 2
    _decode_max_payload_length = 492

    def __init__(self, name, payload):
        if type(payload) != bytes:
            raise TypeError("Payload must be an instance of <bytes>.")

        if type(name) != str and type(name) != bytes:
            raise TypeError("Name must be an instance of <str> or <bytes>.")

        self._name = name.encode() if type(name) == str else name
        self.payload = payload

        if (
            len(self._name) + len(self.payload)
            > self._decode_max_payload_length
        ):
            raise ValueError(
                "Name and payload size must be less than or equal to "
                f"{self._decode_max_payload_length} bytes"
            )

    @property
    def name(self):
        return self._name.decode()

    @name.setter
    def name(self, s):
        if type(s) != bytes and type(s) != str:
            raise TypeError("Name must be either a string or bytes.")
        self._name = s.encode() if type(s) == str else s

    def _encode_payload(self):
        return self._encode_struct(
            f">BH{len(self.payload)}s{len(self._name)}s",
            len(self.payload),
            len(self._name),
            self.payload,
            self._name,
        )

    @classmethod
    def _decode_payload(cls, octets, errors):
        len_payload, len_name, data = cls._decode_struct(
            f">BH{len(octets)-3}s", octets
        )
        payload, _name = cls._decode_struct(f">{len_payload}s{len_name}s", data)
        return cls(_name, payload)


ndef.Record.register_type(BytesRecord)
