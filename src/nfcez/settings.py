from os import getenv

NFCEZ_DISABLE_LOGGING = (
    getenv("NFCEZ_DISABLE_LOGGING", default="false").lower() == "true"
)
NFCEZ_LOG_LEVEL = getenv("NFCEZ_LOG_LEVEL", default="debug")
NFCEZ_LOG_FORMAT = getenv(
    "NFCEZ_LOG_FORMAT",
    default="{asctime} [{levelname}] {name} -- {message}",
)
NFCEZ_LOG_FORMAT_STYLE = getenv("NFCEZ_LOG_FORMAT_STYLE", default="{")
