build: clean
	pipenv run python -m build

clean:
	rm -rf ./build ./dist ./src/nfcez.egg-info ./.coverage ./.pytest_cache ./htmlcov

install:
	pipenv install

install-dev:
	pipenv install --dev

dev:
	pipenv run jupyter lab

publish: build
	pipenv run python -m twine upload -r testpypi dist/*


.PHONY: clean publish install build dev
